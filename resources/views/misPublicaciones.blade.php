<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Mis publicaciones</title>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
		integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
		integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('css/glyphicon.min.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">
</head>

<body>
	<div class="container-fluid px-0 mb-5">
		<div class="row">
			<div class="col-md-12 pr-0">
				<nav class="navbar navbar-expand-sm navbar-light bg-light">
					<button class="navbar-toggler mb-2" type="button" data-toggle="collapse"
						data-target="#bs-example-navbar-collapse-1">
						<span class="navbar-toggler-icon"></span>
					</button>
					<a class="navbar-brand" href="index.blade.php">
						<h2>Inicio</h2>
					</a>
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="navbar-nav">
							<li class="nav-item active">

							</li>
						</ul>
						<ul class="navbar-nav d-none d-lg-block">
							<form class="form-inline">
								<input class="form-control mr-md-2" type="text" placeholder="buscar producto">
								<a href="resultadosBusqueda.blade.php" class="btn btn-primary my-2 my-md-0">Buscar</a>
							</form>
						</ul>
						<li id="miPerfil"
							class="list-group-item d-flex d-sm-inline justify-content-between align-items-center border-0 mr-1">
							<div class="dropdown">
								<a class="btn btn-secondary dropdown-toggle " href="#" role="button"
									id="dropdownMenuCarrito" data-toggle="dropdown" aria-haspopup="true"
									aria-expanded="false">Jorge Alberto Gomez</a>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuMiPerfil">
									<a class="dropdown-item" href="miPerfil.html">Ver mi perfil</a>
									<a class="dropdown-item" href="publicarProducto.blade.php">Publicar producto</a>
									<a class="dropdown-item" href="calificarCompra.blade.php">Calificaciones</a>
									<a class="dropdown-item" href="gestionarBilletera.blade.php">Gestionar billetera</a>
									<a class="dropdown-item" href="miPuntuacion.html">Ver mi puntuación</a>
									<div class="dropdown-divider"></div>
									<a class="dropdown-item" href="index.blade.php">Cerrar sesión</a>
								</div>
							</div>
						</li>
						<ul class="navbar-nav ml-md-auto">
							<li class="list-group-item d-flex d-sm-inline justify-content-between border-0 px-0">
								<div class="form-group mb-0 pt-3">
									<div class="custom-control custom-switch">
										<input type="checkbox" class="custom-control-input" id="customSwitch1"
											checked="" onclick="cambiarPerfil();">
										<label id="textoPerfil" class="custom-control-label" for="customSwitch1">Vista
											comprador</label>
									</div>
								</div>
							</li>
							<li id="listaCarrito"
								class="list-group-item d-flex d-sm-inline justify-content-between align-items-center border-0 mr-1">
								<div class="dropdown">
									<a class="btn btn-secondary dropdown-toggle" href="#" role="button"
										id="dropdownMenuCarrito" data-toggle="dropdown" aria-haspopup="true"
										aria-expanded="false">Mi carrito<span
											class="badge badge-success badge-pill ml-1">16</span></a>
									<div class="dropdown-menu" aria-labelledby="dropdownMenuCarrito">
										<a class="dropdown-item" href="#">Detalle</a>
										<a class="dropdown-item" href="#">Vaciar</a>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</nav>
				<nav>
					<ul class="navbar-nav d-md-block d-lg-none mt-3">
						<li>
							<form class="form-inline mx-3 mb-3">
								<input class="form-control mr-sm-2 w-75" type="text" placeholder="buscar producto">
								<button class="btn btn-primary my-2 my-sm-0" type="submit">Buscar</button>
							</form>
						</li>
					</ul>
				</nav>
				<div class="jumbotron mb-4">
					<h1 class="display-3 text-right">Mi perfil!</h1>
					<p class="lead text-right">Desde acá vas a poder administrar tus publicaciones.</p>
					<hr class="my-4">
					<p class="text-right">Con solo dos clicks podés editar tus preferencias!</p>
				</div>
				<div class="mx-3">
					<!-- PREFERENCIAS DEL USUARIO -->
					<div class="card border-secondary  pb-2 pt-3 text-center">
					<p>CONFIGURACION ACTUAL DE PRECIOS: </p> <h5> PRECIO MINIMO</h5>
					</div>

					<!-- PREFERENCIAS DEL USUARIO | CONFIGURACION DE PRECIOS-->
					<div class="row mt-5">
						<div class="col-md-12 col-lg-4 mb-4">
							<div class="shadow container pb-2 pt-2">
								<div class="mb-3 text-center">
									<div class="card-body ">
										<h4 class="card-title-center text-center mt-3">CONFIGURACION DE PRECIOS</h4>
										<div class="form-group mt-2">
											<p class="card-text mt-4">Desde acá podes cambiar todos tus precios actuales
												a
												valor base, mínimo ó máximo. Recordá que esto impacta en todas las
												publicaciones.</p>
											<div class="input-group mt-4">
												<div class="input-group">
													<select class="custom-select mt-2" id="inputGroupSelect03">
														<!-- Para no mostrar en otro input el valor actual seleccinoado -->
														<option value="" >Elegí un precio..</option>
														<option value="Precio base">Precio base</option>
														<option value="Precio mínimo">Precio mínimo</option>
														<option value="Precio máximo">Precio máximo</option>
													</select>
												</div>
											</div>
										</div>
										<!-- BOTONERA FORMULARIO -->
										<button class="btn btn-success btn-block" type="button"><span
												class="glyphicon glyphicon-ok glyphicon-ld"></button>
									</div>
								</div>
							</div>
						</div>
						<!-- PREFERENCIAS DEL USUARIO | AUMENTO DE PRECIOS-->
						<div class="col-md-12 col-lg-4 mb-4">
							<div class="shadow container pb-2 pt-2">
								<div class="mb-3 text-center">
									<div class="card-body ">
										<h4 class="card-title-center text-center mt-3">INCREMENTO DE PRECIOS</h4>
										<div class="form-group mt-2">
											<p class="card-text mt-4">Desde acá podes incrementar el precio de las
												publicaciones. Recordá que el aumento impacta en todas las publicaciones
												y
												en cada uno de sus precios (base, mínimo y máximo).</p>
											<div class="input-group mb-3">
												<input type="number" class="form-control" placeholder="Porcentaje"
													aria-label="Sizing example input"
													aria-describedby="inputGroup-sizing-default">
												<div class="input-group-prepend">
													<span class="input-group-text"
														id="inputGroup-sizing-default">%</span>
												</div>
											</div>
										</div>
										<!-- BOTONERA FORMULARIO -->
										<button class="btn btn-success btn-block" type="button"><span
												class="glyphicon glyphicon-ok glyphicon-ld"></button>
									</div>
								</div>
							</div>
						</div>
						<!-- PREFERENCIAS DEL USUARIO | DECREMENTO DE PRECIOS-->
						<div class="col-md-12 col-lg-4 mb-4">
							<div class="shadow container pb-2 pt-2">
								<div class="mb-3 text-center">
									<div class="card-body ">
										<h4 class="card-title-center text-center mt-3">DECREMENTO DE PRECIOS</h4>
										<div class="form-group mt-2">
											<p class="card-text mt-4">Desde acá podes decrementar el precio de las
												publicaciones. Recordá que el aumento impacta en todas las publicaciones
												y
												en cada uno de sus precios (base, mínimo y máximo).</p>
											<div class="input-group mb-3">
												<input type="number" class="form-control" placeholder="Porcentaje"
													aria-label="Sizing example input"
													aria-describedby="inputGroup-sizing-default">
												<div class="input-group-prepend">
													<span class="input-group-text"
														id="inputGroup-sizing-default">%</span>
												</div>
											</div>
										</div>
										<!-- BOTONERA FORMULARIO -->

										<button class="btn btn-success btn-block" type="button"><span
												class="glyphicon glyphicon-ok glyphicon-ld"></button>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- ALERT DE BOOTSTRAP
				 <div class="alert alert-success text-center alert-dismissible fade show" role="alert">
					<strong>El monto ha sido agregado en su billetera</strong>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				  </div>
-->
				<div class="mt-3">
					<h3 class="text-center">Mis publicaciones</h4>
				</div>
				<!-- PESTAÑA DE PRODUCTOS-->
				<ul class="nav nav-tabs container mt-3 ml-3">
					<li class="nav-item">
						<a class="nav-link active" data-toggle="tab" href="#publicacionesActivas">Activos</a>
					</li>
					<li class="nav-item">
						<a class="nav-link " data-toggle="tab" href="#publicacionesSinStock">Sin stock</a>
					</li>
					<li class="nav-item">
						<a class="nav-link " data-toggle="tab" href="#publicacionesInactivas">Inactivos</a>
					</li>
				</ul>
				<div id="myTabContent" class="tab-content">
					<div class="tab-pane fade active show" id="publicacionesActivas">
						<!--PRIMER PESTAÑA - PUBLICACIONES ACTIVAS-->
						<div class="mx-3 card border-secondary">
							<table class="mr-2 table table-hover table-bordered">
								<thead class="thead-dark mr-3">
									<tr>
										<th class="text-center">imagen</th>
										<th scope="col" class="text-center">Nombre</th>
										<th scope="col" class="text-center">Precio base</th>
										<th scope="col" class="text-center">Precio mínimo</th>
										<th scope="col" class="text-center">Precio máximo</th>
										<th scope="col" class="text-center">Precio actual</th>
										<th scope="col" class="text-center">Stock actual</th>
										<th scope="col" class="text-center">Modificar stock</th>
										<th scope="col" class="text-center">Acciones</th>
									</tr>
								</thead>
								<tbody>
									<tr class="table-active">
									<tr>
										<th scope="row"><img src="../img/imagen1.jpg" class="card-img-top" alt="" style="width:80px!important; height:80px!important"></th>
										<td class="text-center">nombre</td>
										<td class="text-center">Celular</td>
										<td class="text-center">2500</td>
										<td class="text-center">2000</td>
										<td class="text-center">3000</td>
										<td class="text-center">5</td>
										<td>
											<div class="input-group ">
												<input type="number" class="form-control " placeholder="Nuevo stock"
													aria-label="Recipient's username" aria-describedby="button-addon2">
												<div class="input-group-append">
													<button class="btn btn-success" type="button"
														id="button-addon2"><span
															class="glyphicon glyphicon-pencil"></span></button>
												</div>
											</div>
										</td>
										<td>
											<a href="detallesDeLaPublicacion" class="btn btn-success btn-block"><span
													class="glyphicon glyphicon-eye-open"></span></a>
										</td>
									</tr>
									<tr>
										<th scope="row"><img src="../img/imagen2.png" class="card-img-top" alt="" style="width:80px!important; height:80px!important"></th>
										<td class="text-center">nombre</td>
										<td class="text-center">Celular</td>
										<td class="text-center">2500</td>
										<td class="text-center">2000</td>
										<td class="text-center">3000</td>
										<td class="text-center">5</td>
										<td>
											<div class="input-group ">
												<input type="number" class="form-control " placeholder="Nuevo stock"
													aria-label="Recipient's username" aria-describedby="button-addon2">
												<div class="input-group-append">
													<button class="btn btn-success" type="button"
														id="button-addon2"><span
															class="glyphicon glyphicon-pencil"></span></button>
												</div>
											</div>
										<td>
											<a href="detallesDeLaPublicacion" class="btn btn-success btn-block"><span
													class="glyphicon glyphicon-eye-open	"></span></a>
										</td>
									</tr>
								</tbody>
							</table>
							<!-- PAGINADOR DE LA TABLA-->
							<div class="row justify-content-center centrarPaginador">
								<div class="col-12">
									<ul class="pagination">
										<li class="page-item disabled">
											<a class="page-link" href="#">&laquo;</a>
										</li>
										<li class="page-item active">
											<a class="page-link" href="#">1</a>
										</li>
										<li class="page-item">
											<a class="page-link" href="#">2</a>
										</li>
										<li class="page-item">
											<a class="page-link" href="#">3</a>
										</li>
										<li class="page-item">
											<a class="page-link" href="#">4</a>
										</li>
										<li class="page-item">
											<a class="page-link" href="#">5</a>
										</li>
										<li class="page-item">
											<a class="page-link" href="#">&raquo;</a>
										</li>
									</ul>
								</div>
							</div>
							<div class="card-header mt-1 text-center">
								Sólo se muestran 10 resultados por página
							</div>

						</div>
					</div>
					<div class="tab-pane fade" id="publicacionesSinStock">
						<!--SEGUNDA PESTAÑA - PUBLICACIONES SIN STOCK -->
						<div class="alert alert-success text-center alert-dismissible fade show ml-4 mt-4 mr-4"
							role="alert">
							<strong>No existen publicaciones sin stock</strong>
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
					</div>
					<div class="tab-pane fade" id="publicacionesInactivas">
						<!--TERCER PESTAÑA - PUBLICACIONES INACTIVAS-->
						<div class="alert alert-success text-center alert-dismissible fade show ml-4 mt-4 mr-4"
							role="alert">
							<strong>No existen publicaciones inactivos</strong>
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="{{ URL::asset('js/jquery.min.js') }}" type="text/javascript"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
			integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
			crossorigin="anonymous"></script>
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
			integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
			crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
			integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
			crossorigin="anonymous"></script>
		<script src="{{ URL::asset('js/scripts.js') }}" type="text/javascript"></script>
</body>

</html>