<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Gestionar billetera</title>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
		integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
		integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('css/glyphicon.min.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">
</head>

<body>
	<div class="container-fluid px-0">
		<div class="row">
			<div class="col-md-12 pr-0">
				<nav class="navbar navbar-expand-sm navbar-light bg-light">
					<button class="navbar-toggler mb-2" type="button" data-toggle="collapse"
						data-target="#bs-example-navbar-collapse-1">
						<span class="navbar-toggler-icon"></span>
					</button>
					<a class="navbar-brand" href="index.blade.php">
						<h2>Inicio</h2>
					</a>
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="navbar-nav">
							<li class="nav-item active">

							</li>
						</ul>
						<ul class="navbar-nav d-none d-lg-block">
							<form class="form-inline">
								<input class="form-control mr-md-2" type="text" placeholder="buscar producto">
								<a href="resultadosBusqueda.blade.php" class="btn btn-primary my-2 my-md-0">Buscar</a>
							</form>
						</ul>
						<li id="miPerfil"
							class="list-group-item d-flex d-sm-inline justify-content-between align-items-center border-0 mr-1">
							<div class="dropdown">
								<a class="btn btn-secondary dropdown-toggle " href="#" role="button"
									id="dropdownMenuCarrito" data-toggle="dropdown" aria-haspopup="true"
									aria-expanded="false">Jorge Alberto Gomez</a>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuMiPerfil">
									<a class="dropdown-item" href="miPerfil.html">Ver mi perfil</a>
									<a class="dropdown-item" href="publicarProducto.html">Publicar producto</a>
									<a class="dropdown-item" href="calificarProducto.blade.php">Calificaciones</a>
									<a class="dropdown-item" href="gestionarBilletera.blade.php">Gestionar billetera</a>
									<a class="dropdown-item" href="miPuntuacion.html">Ver mi puntuación</a>
									<div class="dropdown-divider"></div>
									<a class="dropdown-item" href="index.blade.php">Cerrar sesión</a>
								</div>
							</div>
						</li>
						<ul class="navbar-nav ml-md-auto">
							<li class="list-group-item d-flex d-sm-inline justify-content-between border-0 px-0">
								<div class="form-group mb-0 pt-3">
									<div class="custom-control custom-switch">
										<input type="checkbox" class="custom-control-input" id="customSwitch1"
											checked="" onclick="cambiarPerfil();">
										<label id="textoPerfil" class="custom-control-label" for="customSwitch1">Vista
											comprador</label>
									</div>
								</div>
							</li>
							<li id="listaCarrito"
								class="list-group-item d-flex d-sm-inline justify-content-between align-items-center border-0 mr-1">
								<div class="dropdown">
									<a class="btn btn-secondary dropdown-toggle" href="#" role="button"
										id="dropdownMenuCarrito" data-toggle="dropdown" aria-haspopup="true"
										aria-expanded="false">Mi carrito<span
											class="badge badge-success badge-pill ml-1">16</span></a>
									<div class="dropdown-menu" aria-labelledby="dropdownMenuCarrito">
										<a class="dropdown-item" href="#">Detalle</a>
										<a class="dropdown-item" href="#">Vaciar</a>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</nav>
				<nav>
					<ul class="navbar-nav d-md-block d-lg-none mt-3">
						<li>
							<form class="form-inline mx-3 mb-3">
								<input class="form-control mr-sm-2 w-75" type="text" placeholder="buscar producto">
								<button class="btn btn-primary my-2 my-sm-0" type="submit">Buscar</button>
							</form>
						</li>
					</ul>
				</nav>
				<div class="jumbotron">
					<h1 class="display-3 text-right">GESTIONAR BILLETERA!</h1>
					<p class="lead text-right">Desde acá vas a poder depositar o retirar dinero de tu billetera virtual.
					</p>
					<hr class="my-4">
					<p class="text-right">Podés hacer estas operaciones en solo dos clicks!</p>
				</div>
			</div>
		</div>
		<!-- FORM GESTIONAR BILLETERA -->
		<div class="row mx-2">
			<div class="col-8">
				<!-- TABLA MOVIMIENTOS BILLETERA VIRTUAL -->

				<div class="card border-secondary mt-3 mb-5">
					<div class="card-body">
						<h4 class="card-title-center text-center mt-3">DETALLE DE MOVIMIENTOS</h4>
					</div>
					<table class="table table-hover table-bordered mb-3">
						<thead class="thead-dark">
							<tr>
								<th class="text-center">FECHA</th>
								<th scope="col" class="text-center">TIPO DE OPERACION</th>
								<th scope="col" class="text-center">IMPORTE DE OPERACION</th>
								<th scope="col" class="text-center">SALDO</th>
							</tr>
						</thead>
						<tbody>
							<tr class="table-active">
							<tr>
								<th class="text-center" scope="row">7/9/2019</th>
								<td class="text-center">Depósito</td>
								<td class="text-center">2300</td>
								<td class="text-center">2300</td>
							</tr>
							<tr>
								<th class="text-center" scope="row">8/9/2019</th>
								<td class="text-center">Débito</td>
								<td class="text-center">-500</td>
								<td class="text-center">800</td>
							</tr>
							<tr>
								<th class="text-center" scope="row">10/9/2019</th>
								<td class="text-center">Depósito</td>
								<td class="text-center">1000</td>
								<td class="text-center">1800</td>
							</tr>
							<tr>
								<th class="text-center" scope="row">15/9/2019</th>
								<td class="text-center">Débito</td>
								<td class="text-center">-300</td>
								<td class="text-center">1500</td>
							</tr>
						</tbody>
					</table>
					<!-- PAGINADOR DE LA TABLA-->
					<div class="row justify-content-center centrarPaginador">
						<div class="col-12">
							<ul class="pagination">
								<li class="page-item disabled">
									<a class="page-link" href="#">&laquo;</a>
								</li>
								<li class="page-item active">
									<a class="page-link" href="#">1</a>
								</li>
								<li class="page-item">
									<a class="page-link" href="#">2</a>
								</li>
								<li class="page-item">
									<a class="page-link" href="#">3</a>
								</li>
								<li class="page-item">
									<a class="page-link" href="#">4</a>
								</li>
								<li class="page-item">
									<a class="page-link" href="#">5</a>
								</li>
								<li class="page-item">
									<a class="page-link" href="#">&raquo;</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="card-header mt-1 text-center">
						Sólo se muestran 10 resultados por página
					</div>
				</div>
			</div>
			<div class="col-4">
				<div class="shadow mt-3 pb-2 pt-2">
					<div class="mb-3 text-center">
						<div class="card-body ">
							<h4 class="card-title-center text-center mt-3">BILLETERA VIRTUAL</h4>
							@if(count($errors) > 0)
							<div class="alert alert-warning mt-3" role="alert">
								<ul>
								
									@foreach($errors->all() as $error)
									<p>{{ $error }} </p> 
									@endforeach
								</ul>
							</div>
							@endif
							@if(session('mensaje'))	
							<div class="alert alert-success mt-3" role="alert">
								{{ session('mensaje') }}
							</div>
							@endif
							<form action="{{ route('movimiento.crear') }}" method="POST" name="formBilletera">
								@csrf
								<p class="card-text mt-4">Ingresá un monto a debitar o acreditar en tu billetera.</p>
								<div class="input-group mt-4">								
								<div class="form-group container">
									<div class="input-group-prepend">
										<span class="input-group-text">$</span>
										<input class="form-control form-control-lg " type="number"  step="0.01" value="{{ old('monto') }}" placeholder="Monto" name="monto">
									</div>									
								</div>		
								<div class="form-group container">
									<select  id="comboTipo" name="descripcion" class=" custom-select  custom-select-lg ">
											<option value="" >Tipo de operación..</option>
											<option value="Depósito">Depósito</option>
											<option value="Débito">Débito</option>
									</select>
								</div>
								<!-- BOTONERA FORMULARIO -->
								<div class="form-group mt-3 container">
									<div class="card-header ">			
											<button class="btn btn-success btn-block" type="submit"><span
											class="glyphicon glyphicon-ok glyphicon-ld"></button>
									</div>
								</div>	
						  </form>	
						</div>
					</div>
				</div>
		</div>
	</div>
	</div>
	<script src="{{ URL::asset('js/jquery.min.js') }}" type="text/javascript"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script href="{{ URL::asset('js/scripts.js') }}" type="text/javascript"></script>
</body>
</html>