<div class="row">
        <div class="col-md-12 pr-0">
            <nav class="navbar navbar-expand-sm navbar-light bg-light"> 
                <button class="navbar-toggler mb-2" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="navbar-toggler-icon"></span>
                </button> 
                <a class="navbar-brand" href="index.html">
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            
                        </li>
                    </ul>
                    <ul class="navbar-nav d-none d-lg-block">
                        <form class="form-inline">
                            <input class="form-control mr-md-2" type="text" placeholder="buscar producto"> 
                            <a href="resultadosBusqueda.html" class="btn btn-primary my-2 my-md-0">Buscar</a>
                        </form>
                    </ul>
                    <li id="miPerfil" class="list-group-item d-flex d-sm-inline justify-content-between align-items-center border-0 mr-1">
                            <div class="dropdown">
                                <a class="btn btn-secondary dropdown-toggle " href="#" role="button" id="dropdownMenuCarrito" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Jorge Alberto Gomez</a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuMiPerfil" >
                                    <a class="dropdown-item" href="miPerfil.html">Ver mi perfil</a>
                                    <a class="dropdown-item" href="publicarProducto.html">Publicar producto</a>						
                                    <a class="dropdown-item" href="calificarProducto.html">Calificaciones</a>
                                    <a class="dropdown-item" href="gestionarBilletera.html">Gestionar billetera</a>
                                    <a class="dropdown-item" href="miPuntuacion.html">Ver mi puntuación</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="index.html">Cerrar sesión</a>
                                </div>
                            </div>
                    </li>
                    <ul class="navbar-nav ml-md-auto">
                        <li class="list-group-item d-flex d-sm-inline justify-content-between border-0 px-0">
                            <div class="form-group mb-0 pt-3">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="customSwitch1" checked="" onclick="cambiarPerfil();">
                                    <label id="textoPerfil" class="custom-control-label" for="customSwitch1">Vista comprador</label>
                                </div>
                            </div>
                        </li>
                        <li id="listaCarrito" class="list-group-item d-flex d-sm-inline justify-content-between align-items-center border-0 mr-1">
                            <div class="dropdown">
                                <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuCarrito" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Mi carrito<span class="badge badge-success badge-pill ml-1">16</span></a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuCarrito">
                                    <a class="dropdown-item" href="#">Detalle</a>
                                    <a class="dropdown-item" href="#">Vaciar</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
            <nav>
                <ul class="navbar-nav d-md-block d-lg-none mt-3">
                    <li>
                        <form class="form-inline mx-3 mb-3">
                            <input class="form-control mr-sm-2 w-75" type="text" placeholder="buscar producto"> 
                            <button class="btn btn-primary my-2 my-sm-0" type="submit">Buscar</button>
                        </form>
                    </li>
                </ul>
            </nav>
            <div class="jumbotron">
                <h1 class="display-3 text-right">GESTIONAR BILLETERA</h1>
                <p class="lead text-right">Desde acá vas a poder depositar o debitar dinero de tu billetera virtual.</p>
                
                
            </div>
        </div>
    </div>