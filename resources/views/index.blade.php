<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Inicio</title>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
		integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
		integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('css/glyphicon.min.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">
</head>

<body>
	<div class="container-fluid px-0">
		<div class="row">
			<div class="col-md-12 pr-0">
				<nav class="navbar navbar-expand-sm navbar-light bg-light">
					<button class="navbar-toggler mb-2" type="button" data-toggle="collapse"
						data-target="#bs-example-navbar-collapse-1">
						<span class="navbar-toggler-icon"></span>
					</button>
					<a class="navbar-brand" href="index.blade.php">
						<span class="glyphicon glyphicon-home glyphicon-xl px-3">
					</a>
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="navbar-nav">
							<li class="nav-item active">
								<button id="modal-login" class="btn btn-outline-success mb-2"
									href="#modal-container-login" role="button" data-toggle="modal">INICIAR SESION</a>
							</li>
						</ul>
						<ul class="navbar-nav d-none d-lg-block">
							<form class="form-inline">
								<input class="form-control mr-md-2" type="text" placeholder="buscar producto">
								<button class="btn btn-primary my-2 my-md-0" type="submit"><span class="glyphicon glyphicon-search glyphicon-md"></span></button>
  								<button class="btn btn-primary mx-2" type="button" data-toggle="collapse" data-target="#filtros" aria-expanded="false" aria-controls="filtros"><span class="glyphicon glyphicon-tasks glyphicon-md"></span></button>
							</form>
						</ul>
						<ul class="navbar-nav ml-md-auto">
							<li id="listaCarrito"
								class="list-group-item d-flex d-sm-inline justify-content-between align-items-center border-0 mr-1">
								<div class="dropdown">
									<a class="btn btn-secondary dropdown-toggle" href="#" role="button"
										id="dropdownMenuCarrito" data-toggle="dropdown" aria-haspopup="true"
										aria-expanded="false">
										<span class="glyphicon glyphicon-shopping-cart glyphicon-lg"
											aria-hidden="true"></span>
										<span class="badge badge-success badge-pill ml-1">16</span>
									</a>
									<div class="dropdown-menu" aria-labelledby="dropdownMenuCarrito">
										<a class="dropdown-item" href="#"><span
												class="glyphicon glyphicon-cog px-2"></span> Detalle</a>
										<a class="dropdown-item" href="#"><span
												class="glyphicon glyphicon-trash px-2"></span> Vaciar</a>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</nav>
				<nav>
					<ul class="navbar-nav d-md-block d-lg-none mt-3">
						<li>
							<form class="form-inline mx-3 mb-3">
								<input class="form-control mr-sm-2 w-50" type="text" placeholder="buscar producto">
								<button class="btn btn-primary my-2 my-sm-0" type="submit"><span class="glyphicon glyphicon-search glyphicon-md"></span></button>
								<button class="btn btn-primary mx-2" type="button" data-toggle="collapse" data-target="#filtros" aria-expanded="false" aria-controls="filtros"><span class="glyphicon glyphicon-tasks glyphicon-md"></span></button>
							</form>
						</li>
					</ul>
				</nav>
				<div class="row">
					<div class="col-12">
						<div class="collapse multi-collapse" id="filtros">
							<div class="pb-2 pt-2">
								<div class="mb-3 text-center">
									<div class="card-body ">
										<div class="form-group mt-3">
											<div class="row">
												<div class="col-md-12 col-lg-6">
													<p class="text-left text-transform-initial">Seleccionar categoria</p>
													<select class="custom-select d-inline-block">
														<option selected="">Otros</option>
														<option value="1">Electrónica</option>
														<option value="2">Indumentaria</option>
														<option value="3">Vehículos</option>
													</select>
												</div>
												<div class="col-md-12 col-lg-6">
													<p class="text-left text-transform-initial">Seleccionar subcategoria</p>
													<select class="custom-select d-inline-block" disabled>
														<option selected=""></option>
													</select>
												</div>
											</div>											
										</div>
										<div class="form-group">
											<p class="text-left text-transform-initial mb-1">Rango de precios</p>
											<div class="row">
												<div class="col-md-12 col-lg-6">
													<label class="control-label text-right d-block">mayor a</label>
													<div class="input-group">
														<div class="input-group-prepend">
															<span class="input-group-text">$</span>
														</div>
														<input type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
														<div class="input-group-append">
															<span class="input-group-text">.00</span>
														</div>
													</div>
												</div>
												<div class="col-md-12 col-lg-6">
													<label class="control-label text-right d-block">menor a</label>
													<div class="input-group">
														<div class="input-group-prepend">
															<span class="input-group-text">$</span>
														</div>
														<input type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
														<div class="input-group-append">
															<span class="input-group-text">.00</span>
														</div>
													</div>
												</div>
											</div>
										</div>
										<button type="button" class="btn btn-success btn-block mt-2"><span class="glyphicon glyphicon-ok glyphicon-lg"></span></button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="jumbotron">
					<h1 class="display-3 text-right">Bienvenido!</h1>
					<p class="lead text-right">En este sitio vas a encontrar lo que necesitas en pocos clicks, animate!
					</p>
					<hr class="my-4">
					<p class="text-right">Comenza a navegar y agrega lo que necesites a tu carrito</p>
					<p class="lead text-right mr-5">
						<a id="modal-registrar" class="btn btn-outline-warning" href="#modal-container-registrar" role="button" data-toggle="modal">Registrarme</a>
					</p>
				</div>
			</div>
		</div>
		<div class="row mx-3">
			<div class="card-columns">
				<div class="card productoHover">
					<img src="../img/imagen2.png" class="card-img-top" alt="">
					<div class="card-body">
						<h3 class="card-title text-primary text-right color-text-success">$12345.99</h3>
						<h5 class="card-title text-primary">Monitor LG 29"</h5>
						<p class="card-text text-primary">LED de 29" marca LG en excelente condición.</p>
						<p class="card-text text-right"><small class="text-muted">Publicado hace 8 días</small></p>
					</div>
					<button type="button" class="btn btn-success btn-block"><span
							class="glyphicon glyphicon-gift glyphicon-lg px-3"></span>Comprar</button>
				</div>
				<div class="card productoHover">
					<img src="../img/imagen1.jpg" class="card-img-top" alt="">
					<div class="card-body">
							<h3 class="card-title text-primary text-right color-text-success">$12345.99</h3>
							<h5 class="card-title text-primary">Monitor LG 29"</h5>
							<p class="card-text text-primary">LED de 29" marca LG en excelente condición.</p>
							<p class="card-text text-right"><small class="text-muted">Publicado hace 8 días</small></p>
					</div>
					<button type="button" class="btn btn-success btn-block mt-2"><span
							class="glyphicon glyphicon-gift glyphicon-lg px-3"></span>Comprar</button>
				</div>
				<div class="card productoHover">
					<img src="../img/imagen2.png" class="card-img-top" alt="">
					<div class="card-body">
							<h3 class="card-title text-primary text-right color-text-success">$12345.99</h3>
							<h5 class="card-title text-primary">Monitor LG 29"</h5>
							<p class="card-text text-primary">LED de 29" marca LG en excelente condición.</p>
							<p class="card-text text-right"><small class="text-muted">Publicado hace 8 días</small></p>
					</div>
					<button type="button" class="btn btn-success btn-block mt-2"><span
							class="glyphicon glyphicon-gift glyphicon-lg px-3"></span>Comprar</button>
				</div>
				<div class="card productoHover">
					<img src="../img/imagen1.jpg" class="card-img-top" alt="">
					<div class="card-body">
							<h3 class="card-title text-primary text-right color-text-success">$12345.99</h3>
							<h5 class="card-title text-primary">Monitor LG 29"</h5>
							<p class="card-text text-primary">LED de 29" marca LG en excelente condición.</p>
							<p class="card-text text-right"><small class="text-muted">Publicado hace 8 días</small></p>
					</div>
					<button type="button" class="btn btn-success btn-block mt-2"><span
							class="glyphicon glyphicon-gift glyphicon-lg px-3"></span>Comprar</button>
				</div>
				<div class="card productoHover">
					<img src="../img/imagen1.jpg" class="card-img-top" alt="">
					<div class="card-body">
							<h3 class="card-title text-primary text-right color-text-success">$12345.99</h3>
							<h5 class="card-title text-primary">Monitor LG 29"</h5>
							<p class="card-text text-primary">LED de 29" marca LG en excelente condición.</p>
							<p class="card-text text-right"><small class="text-muted">Publicado hace 8 días</small></p>
					</div>
					<button type="button" class="btn btn-success btn-block mt-2"><span
							class="glyphicon glyphicon-gift glyphicon-lg px-3"></span>Comprar</button>
				</div>
				<div class="card productoHover">
					<img src="../img/imagen2.png" class="card-img-top" alt="">
					<div class="card-body">
							<h3 class="card-title text-primary text-right color-text-success">$12345.99</h3>
							<h5 class="card-title text-primary">Monitor LG 29"</h5>
							<p class="card-text text-primary">LED de 29" marca LG en excelente condición.</p>
							<p class="card-text text-right"><small class="text-muted">Publicado hace 8 días</small></p>
					</div>
					<button type="button" class="btn btn-success btn-block mt-2"><span
							class="glyphicon glyphicon-gift glyphicon-lg px-3"></span>Comprar</button>
				</div>
				<div class="card productoHover">
					<img src="../img/imagen3.png" class="card-img-top" alt="">
					<div class="card-body">
							<h3 class="card-title text-primary text-right color-text-success">$12345.99</h3>
							<h5 class="card-title text-primary">Monitor LG 29"</h5>
							<p class="card-text text-primary">LED de 29" marca LG en excelente condición.</p>
							<p class="card-text text-right"><small class="text-muted">Publicado hace 8 días</small></p>
					</div>
					<button type="button" class="btn btn-success btn-block mt-2"><span
							class="glyphicon glyphicon-gift glyphicon-lg px-3"></span>Comprar</button>
				</div>
				<div class="card productoHover">
					<img src="../img/imagen1.jpg" class="card-img-top" alt="">
					<div class="card-body">
							<h3 class="card-title text-primary text-right color-text-success">$12345.99</h3>
							<h5 class="card-title text-primary">Monitor LG 29"</h5>
							<p class="card-text text-primary">LED de 29" marca LG en excelente condición.</p>
							<p class="card-text text-right"><small class="text-muted">Publicado hace 8 días</small></p>
					</div>
					<button type="button" class="btn btn-success btn-block mt-2"><span
							class="glyphicon glyphicon-gift glyphicon-lg px-3"></span>Comprar</button>
				</div>
				<div class="card productoHover">
					<img src="../img/imagen1.jpg" class="card-img-top" alt="">
					<div class="card-body">
						<h5 class="card-title text-primary">Monitor LG 29"</h5>
						<p class="card-text text-primary">LED de 29" marca LG en excelente condición.</p>
						<p class="card-text text-right"><small class="text-muted">Publicado hace 8 días</small></p>
					</div>
					<button type="button" class="btn btn-success btn-block mt-2"><span
							class="glyphicon glyphicon-gift glyphicon-lg px-3"></span>Comprar</button>
				</div>
				<div class="card productoHover">
					<img src="../img/imagen1.jpg" class="card-img-top" alt="">
					<div class="card-body">
							<h3 class="card-title text-primary text-right color-text-success">$12345.99</h3>
							<h5 class="card-title text-primary">Monitor LG 29"</h5>
							<p class="card-text text-primary">LED de 29" marca LG en excelente condición.</p>
							<p class="card-text text-right"><small class="text-muted">Publicado hace 8 días</small></p>
					</div>
					<button type="button" class="btn btn-success btn-block mt-2"><span
							class="glyphicon glyphicon-gift glyphicon-lg px-3"></span>Comprar</button>
				</div>
			</div>
		</div>
		<!-- <div class="row justify-content-center centrarPaginador mt-4">
			<div class="col-12">
				<ul class="pagination">
					<li class="page-item disabled">
						<a class="page-link" href="#">&laquo;</a>
					</li>
					<li class="page-item active">
						<a class="page-link" href="#">1</a>
					</li>
					<li class="page-item">
						<a class="page-link" href="#">2</a>
					</li>
					<li class="page-item">
						<a class="page-link" href="#">3</a>
					</li>
					<li class="page-item">
						<a class="page-link" href="#">4</a>
					</li>
					<li class="page-item">
						<a class="page-link" href="#">5</a>
					</li>
					<li class="page-item">
						<a class="page-link" href="#">&raquo;</a>
					</li>
				</ul>
			</div>
		</div> -->
		<div class="row">
			<div class="col-md-12 p-0 mt-3">
				<div class="jumbotron m-0">
				</div>
			</div>
		</div>

		<div class="modal fade" id="modal-container-login" role="dialog" aria-labelledby="myModallogin"
			aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="myModallogin">Iniciar sesión</h5>
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label for="exampleInputEmail1">Correo electrónico</label>
							<input type="email" class="form-control" id="exampleInputEmail1">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Contraseña</label>
							<input type="password" class="form-control" id="exampleInputPassword1">
						</div>
					</div>
					<div class="modal-footer">
						<a href="index.blade.php" class="btn btn-outline-success btn-block">Ingresar</a>
					</div>
					<a id="modal-registrar" class="btn btn-secondary text-left sinCuenta text-transform-initial"
						href="#modal-container-registrar" role="button" data-toggle="modal"><span class="text-info">Aún
							no tengo cuenta</span></a>
				</div>
			</div>
		</div>

		<div class="modal fade" id="modal-container-registrar" role="dialog" aria-labelledby="mymodalregistrar"
			aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="mymodalregistrar">Registrarme</h5>
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<div class="col-12 px-0">
								<div class="row">
									<div class="col-6">
										<label for="exampleinputname">Nombre</label>
										<input type="text" class="form-control" id="exampleinputname">
									</div>
									<div class="col-6">
										<label for="exampleinputsurname">Apellido</label>
										<input type="text" class="form-control" id="exampleinputsurname">
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="exampleinputbirthdate">Fecha de nacimiento</label>
							<input type="date" class="form-control" id="exampleinputbirthdate">
						</div>
						<div class="form-group">
							<label for="exampleinputemail1">Correo electrónico</label>
							<input type="email" class="form-control" id="exampleinputemail1">
						</div>
						<div class="form-group">
							<label for="exampleinputpassword1">Contraseña</label>
							<input type="password" class="form-control" id="exampleinputpassword1">
						</div>
					</div>
					<div class="modal-footer">
						<a href="index.blade.php" class="btn btn-outline-success btn-block">Registrar</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="{{ URL::asset('js/jquery.min.js') }}" type="text/javascript"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script src="{{ URL::asset('js/scripts.js') }}" type="text/javascript"></script>

</body>

</html>