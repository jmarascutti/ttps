<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Nueva publicación</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
        integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/glyphicon.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">

</head>

<body>
    <div class="container-fluid pl-0 mb-5">
        <div class="row">
            <div class="col-md-12 pr-0">
                <nav class="navbar navbar-expand-sm navbar-light bg-light">
                    <button class="navbar-toggler mb-2" type="button" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <a class="navbar-brand" href="index.html">
                        <h2>Inicio</h2>
                    </a>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="navbar-nav">
                            <li class="nav-item active">

                            </li>
                        </ul>
                        <ul class="navbar-nav d-none d-lg-block">
                            <form class="form-inline">
                                <input class="form-control mr-md-2" type="text" placeholder="buscar producto">
                                <a href="resultadosBusqueda.html" class="btn btn-primary my-2 my-md-0">Buscar</a>
                            </form>
                        </ul>
                        <li id="miPerfil"
                            class="list-group-item d-flex d-sm-inline justify-content-between align-items-center border-0 mr-1">
                            <div class="dropdown">
                                <a class="btn btn-secondary dropdown-toggle " href="#" role="button"
                                    id="dropdownMenuCarrito" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false">Jorge Alberto Gomez</a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuMiPerfil">
                                    <a class="dropdown-item" href="miPerfil.html">Ver mi perfil</a>
                                    <a class="dropdown-item" href="publicarProducto.html">Publicar producto</a>
                                    <a class="dropdown-item" href="calificarProducto.html">Calificaciones</a>
                                    <a class="dropdown-item" href="gestionarBilletera.html">Gestionar billetera</a>
                                    <a class="dropdown-item" href="miPuntuacion.html">Ver mi puntuación</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="index.html">Cerrar sesión</a>
                                </div>
                            </div>
                        </li>
                        <ul class="navbar-nav ml-md-auto">
                            <li class="list-group-item d-flex d-sm-inline justify-content-between border-0 px-0">
                                <div class="form-group mb-0 pt-3">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="customSwitch1"
                                            checked="" onclick="cambiarPerfil();">
                                        <label id="textoPerfil" class="custom-control-label" for="customSwitch1">Vista
                                            comprador</label>
                                    </div>
                                </div>
                            </li>
                            <li id="listaCarrito"
                                class="list-group-item d-flex d-sm-inline justify-content-between align-items-center border-0 mr-1">
                                <div class="dropdown">
                                    <a class="btn btn-secondary dropdown-toggle" href="#" role="button"
                                        id="dropdownMenuCarrito" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">Mi carrito<span
                                            class="badge badge-success badge-pill ml-1">16</span></a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuCarrito">
                                        <a class="dropdown-item" href="#">Detalle</a>
                                        <a class="dropdown-item" href="#">Vaciar</a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
                <nav>
                    <ul class="navbar-nav d-md-block d-lg-none mt-3">
                        <li>
                            <form class="form-inline mx-3 mb-3">
                                <input class="form-control mr-sm-2 w-75" type="text" placeholder="buscar producto">
                                <button class="btn btn-primary my-2 my-sm-0" type="submit">Buscar</button>
                            </form>
                        </li>
                    </ul>
                </nav>
                <div class="jumbotron mb-5">
                    <h1 class="display-3 text-right">Nueva publicación</h1>
                    <p class="lead text-right">Aquí vas a poder dar de alta tus publicaciones.</p>
					<hr class="my-4">
					<p class="text-right">Podés crear publicaciones en pocos clicks!</p>
                </div>
                <div class="row mx-3">
                    <div class="col-12 form-group">
                        <h4><label for="titulo">TÍTULO</label></h4>
                        <input type="text" class="form-control" id="titulo" placeholder="Escribe aquí el título de tu producto">
                    </div>
                    <div class="col-12 form-group">
                        <h4><label for="descripcion">DESCRIPCIÓN</label></h4>
                        <textarea class="form-control" id="descripcion" rows="8" placeholder="Escribe aquí alguna descripción sobre tu producto"></textarea>
                    </div>
                    <div class="col-12 form-group">
                        <div class="row">
                            <div class="col-md-12 col-lg-5 form-group">
                                <h4><label for="categoria">CATEGORÍA</label></h4>
                                <select class="custom-select" id="categoria">
                                    <option>Vehículos</option>
                                    <option>Electronica</option>
                                    <option>Muebles</option>
                                    <option>Indumentaria</option>
                                    <option>Articulos para el hogar</option>
                                </select>
                            </div>
                            <div class="col-md-12 col-lg-5 form-group">
                                <h4><label for="categoria">SUBCATEGORIA</label></h4>
                                <select class="custom-select" id="categoria">
                                    <option>Volantes</option>
                                    <option>Frenos</option>
                                    <option>Neumaticos</option>
                                </select>
                            </div>
                            <div class="col-md-12 col-lg-2 form-group">
                                <h4><label for="stock">STOCK</label></h4>
                                <input type="number" class="form-control" id="stock">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mx-3">
                    <div class="col-12 form-group">
                        <h4><label>ESTABLECE LOS PRECIOS DE TU PRODUCTO</label></h4>
                        <div class="form-row">
                            <div class="form-group col-md-4 text-center">
                                <label for="precioBajo">Precio mínimo</label>
                                <div class="input-group-prepend">
                                    <span class="input-group-text px-2">$</span>
                                    <input type="number" class="form-control pr-1" id="precioBajo">
                                </div>
                            </div>
                            <div class="form-group col-md-4 text-center">
                                <label for="precioBase">Precio base</label>
                                <div class="input-group-prepend">
                                    <span class="input-group-text px-2">$</span>
                                    <input type="number" class="form-control pr-1" id="precioBase">
                                </div>
                            </div>
                            <div class="form-group col-md-4 text-center">
                                <label for="precioAlto">Precio máximo</label>
                                <div class="input-group-prepend">
                                    <span class="input-group-text px-2">$</span>
                                    <input type="number" class="form-control pr-1" id="precioAlto">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mx-3">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-8 form-group">
                                <h4><label for="imagen">IMÁGEN</label></h4>
                                <label>Cantidad mínima: 1 - Cantidad máxima: 5 | Tamaño máximo 10 MB c/u</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="imagen" aria-describedby="inputGroupFileAddon04">
                                    <label class="custom-file-label" for="imagen">Selecciona un archivo</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 form-group">
                                <div class="card-group">
                                    <div class="card mx-1">
                                        <img src="../img/imagen2.png" alt="Los Angeles">
                                        <div class="card-footer text-center">
                                            <button class="btn btn-success btn-block"><span class="glyphicon glyphicon-trash"></span></button>
                                        </div>
                                    </div>
                                    <div class="card border-left mx-1">
                                        <img src="../img/imagen1.jpg" alt="Los Angeles">
                                        <div class="card-footer text-center">
                                            <button class="btn btn-success btn-block"><span class="glyphicon glyphicon-trash"></span></button>
                                        </div>
                                    </div>
                                    <div class="card border-left mx-1">
                                        <img src="../img/imagen1.jpg" alt="Los Angeles">
                                        <div class="card-footer text-center">
                                            <button class="btn btn-success btn-block"><span class="glyphicon glyphicon-trash"></span></button>
                                        </div>
                                    </div>
                                    <div class="card border-left mx-1">
                                        <img src="../img/imagen1.jpg" alt="Los Angeles">
                                        <div class="card-footer text-center">
                                            <button class="btn btn-success btn-block"><span class="glyphicon glyphicon-trash"></span></button>
                                        </div>
                                    </div>
                                    <div class="card border-left mx-1">
                                        <img src="../img/imagen1.jpg" alt="Los Angeles">
                                        <div class="card-footer text-center">
                                            <button class="btn btn-success btn-block"><span class="glyphicon glyphicon-trash"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{ URL::asset('js/jquery.min.js') }}" type="text/javascript"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
        </script>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
        </script>
        <script src="{{ URL::asset('js/scripts.js') }}" type="text/javascript"></script>
    </div>
</body>

</html>