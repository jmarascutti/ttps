<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Calificaciones</title>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
		integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
		integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('css/glyphicon.min.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">
</head>

<body>
	<div class="container-fluid px-0">
		<div class="row">
			<div class="col-md-12 pr-0">
				<nav class="navbar navbar-expand-sm navbar-light bg-light">
					<button class="navbar-toggler mb-2" type="button" data-toggle="collapse"
						data-target="#bs-example-navbar-collapse-1">
						<span class="navbar-toggler-icon"></span>
					</button>
					<a class="navbar-brand" href="index.blade.php">
						<h2>Inicio</h2>
					</a>
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="navbar-nav">
							<li class="nav-item active">

							</li>
						</ul>
						<ul class="navbar-nav d-none d-lg-block">
							<form class="form-inline">
								<input class="form-control mr-md-2" type="text" placeholder="buscar producto">
								<a href="resultadosBusqueda.blade.php" class="btn btn-primary my-2 my-md-0">Buscar</a>
							</form>
						</ul>
						<li id="miPerfil"
							class="list-group-item d-flex d-sm-inline justify-content-between align-items-center border-0 mr-1">
							<div class="dropdown">
								<a class="btn btn-secondary dropdown-toggle " href="#" role="button"
									id="dropdownMenuCarrito" data-toggle="dropdown" aria-haspopup="true"
									aria-expanded="false">Jorge Alberto Gomez</a>
								<div class="dropdown-menu" aria-labelledby="dropdownMenuMiPerfil">
									<a class="dropdown-item" href="miPerfil.html">Ver mi perfil</a>
									<a class="dropdown-item" href="publicarProducto.html">Publicar producto</a>
									<a class="dropdown-item" href="calificarProducto.blade.php">Calificaciones</a>
									<a class="dropdown-item" href="gestionarBilletera.blade.php">Gestionar billetera</a>
									<a class="dropdown-item" href="miPuntuacion.html">Ver mi puntuación</a>
									<div class="dropdown-divider"></div>
									<a class="dropdown-item" href="index.blade.php">Cerrar sesión</a>
								</div>
							</div>
						</li>
						<ul class="navbar-nav ml-md-auto">
							<li class="list-group-item d-flex d-sm-inline justify-content-between border-0 px-0">
								<div class="form-group mb-0 pt-3">
									<div class="custom-control custom-switch">
										<input type="checkbox" class="custom-control-input" id="customSwitch1"
											checked="" onclick="cambiarPerfil();">
										<label id="textoPerfil" class="custom-control-label" for="customSwitch1">Vista
											comprador</label>
									</div>
								</div>
							</li>
							<li id="listaCarrito"
								class="list-group-item d-flex d-sm-inline justify-content-between align-items-center border-0 mr-1">
								<div class="dropdown">
									<a class="btn btn-secondary dropdown-toggle" href="#" role="button"
										id="dropdownMenuCarrito" data-toggle="dropdown" aria-haspopup="true"
										aria-expanded="false">Mi carrito<span
											class="badge badge-success badge-pill ml-1">16</span></a>
									<div class="dropdown-menu" aria-labelledby="dropdownMenuCarrito">
										<a class="dropdown-item" href="#">Detalle</a>
										<a class="dropdown-item" href="#">Vaciar</a>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</nav>
				<nav>
					<ul class="navbar-nav d-md-block d-lg-none mt-3">
						<li>
							<form class="form-inline mx-3 mb-3">
								<input class="form-control mr-sm-2 w-75" type="text" placeholder="buscar producto">
								<button class="btn btn-primary my-2 my-sm-0" type="submit">Buscar</button>
							</form>
						</li>
					</ul>
				</nav>
				<div class="jumbotron mb-5">
					<h1 class="display-3 text-right">Calificación</h1>
					<p class="lead text-right">Desde acá vas a poder calificar un producto comprado recientemente</p>
					<hr class="my-4">
					<p class="text-right">Podés dejar tu opinión en pocos clicks!</p>
				</div>
				<div class="mt-3">
					<h3 class="text-center">Calificá tus compras</h4>
				</div>
				<div id="myTabContent" class="tab-content mt-3">
					<div class="tab-pane fade active show">
						<!-- PESTAÑA - COMPRAS CALIFICADAS-->
						<div class=" ml-4  mr-5 card border-secondary mb-5">
							<table class="mr-2 table table-hover table-bordered">
								<thead class="thead-dark mr-3">
									<tr>
										<th class="text-center">Imagen</th>
										<th scope="col" class="text-center">Fecha</th>
										<th scope="col" class="text-center">Cantidad</th>
										<th scope="col" class="text-center">Descripción</th>
										<th scope="col" class="text-center">Importe</th>
										<th scope="col" class="text-center">Acciones</th>
									</tr>
								</thead>
								<tbody>
									<tr class="table-active">
									<tr>
									<th scope="row" style="width:90px!important"><img src="../img/bycle.png" class="card-img-top" alt="" style="width:80px!important; height:80px!important"></th>
										<td class="text-center">7/9/2019</td>
										<td class="text-center">2</td>
										<td class="text-center">Celular Samsung Galaxy J7</td>
										<td class="text-center">$12000</td>
										<td class="text-center">
											<a href="calificar" class="btn btn-success px-5"><span class="glyphicon glyphicon-thumbs-up glyphicon-lg"></span></a>
										</td>
									</tr>
									<tr>
									<th scope="row" style="width:90px!important"><img src="../img/bycle.png" class="card-img-top" alt="" style="width:80px!important; height:80px!important"></th>
										<td class="text-center">9/9/2019</td>
										<td class="text-center">15</td>
										<td class="text-center">Bolsas de arena</td>
										<td class="text-center">6000</td>
										<td class="text-center">
											<a href="calificar" class="btn btn-success px-5"><span class="glyphicon glyphicon-thumbs-up glyphicon-lg"></span></a>
										</td>
									</tr>
								</tbody>
							</table>
							<!-- PAGINADOR DE LA TABLA-->
							<div class="row justify-content-center centrarPaginador ">
								<div class="col-12">
									<ul class="pagination">
										<li class="page-item disabled">
											<a class="page-link" href="#">&laquo;</a>
										</li>
										<li class="page-item active">
											<a class="page-link" href="#">1</a>
										</li>
										<li class="page-item">
											<a class="page-link" href="#">2</a>
										</li>
										<li class="page-item">
											<a class="page-link" href="#">3</a>
										</li>
										<li class="page-item">
											<a class="page-link" href="#">4</a>
										</li>
										<li class="page-item">
											<a class="page-link" href="#">5</a>
										</li>
										<li class="page-item">
											<a class="page-link" href="#">&raquo;</a>
										</li>
									</ul>
								</div>
							</div>
							<div class="card-header mt-1 text-center">
								Sólo se muestran 10 resultados por página
							</div>
						</div>
					</div>
				</div>
				<!-- MODAL DE CALIFICAR COMPRA -->
				<div class="modal fade" id="modal-container-calificar" role="dialog" aria-labelledby="myModallogin"
					aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="myModallogin">Calificar producto</h5>
								<button type="button" class="close" data-dismiss="modal">
									<span aria-hidden="true">×</span>
								</button>
							</div>
							<form action="{{ route('calificacion.crear') }}" method="POST" name="formCalificacion">
								@csrf
								<div class="modal-body">
									<div class="form-group">
									<label for="exampleTextarea">Escribí un comentario acerca del vendedor y del producto</label>
										<textarea class="form-control" name="comentario" id="exampleTextarea" rows="3" style="margin-top: 0px; margin-bottom: 0px; height: 102px;" placeholder="Comentario.."></textarea>
									</div>
									<div class="form-group">
										<label for="exampleTextarea">Elegí un puntaje acerca de la compra</label>
										<div class="form-check card-header mb-2 text-center">
											<label class="form-check-label">
												<div class="custom-control custom-radio">
													<div class="custom-control custom-radio d-inline-flex mx-2">
														<input type="radio" id="customRadio1" name="radioButton"
															class="custom-control-input mx-1" checked="">
														<label class="custom-control-label" for="customRadio1">1</label>
													</div>
													<div class="custom-control custom-radio d-inline-flex mx-2">
														<input type="radio" id="customRadio2" name="radioButton"
															class="custom-control-input mx-1">
														<label class="custom-control-label" for="customRadio2">2</label>
													</div>
													<div class="custom-control custom-radio d-inline-flex mx-2">
														<input type="radio" id="customRadio3" name="radioButton"
															class="custom-control-input mx-1">
														<label class="custom-control-label" for="customRadio3">3</label>
													</div>
													<div class="custom-control custom-radio d-inline-flex mx-2">
														<input type="radio" id="customRadio4" name="radioButton"
															class="custom-control-input mx-1">
														<label class="custom-control-label" for="customRadio4">4</label>
													</div>
													<div class="custom-control custom-radio d-inline-flex mx-2">
														<input type="radio" id="customRadio5" name="radioButton"
															class="custom-control-input mx-1">
														<label class="custom-control-label" for="customRadio5">5</label>
													</div>
												</div>
										 </div>
									</div>
									<button type="submit" class="btn btn-success btn-block"><span
											class="glyphicon glyphicon-ok glyphicon-lg"></span></button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="{{ URL::asset('js/jquery.min.js') }}" type="text/javascript"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
			integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
			crossorigin="anonymous"></script>
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
			integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
			crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
			integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
			crossorigin="anonymous"></script>
		<script src="{{ URL::asset('js/scripts.js') }}" type="text/javascript"></script>
</body>

</html>