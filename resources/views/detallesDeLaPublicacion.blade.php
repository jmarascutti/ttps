<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Detalles del producto</title>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
		integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
		integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('css/glyphicon.min.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">
</head>

<body>
	<div class="container-fluid px-0">
		<div class="row">
			<div class="col-md-12 pr-0">
				<nav class="navbar navbar-expand-sm navbar-light bg-light">
					<button class="navbar-toggler mb-2" type="button" data-toggle="collapse"
						data-target="#bs-example-navbar-collapse-1">
						<span class="navbar-toggler-icon"></span>
					</button>
					<a class="navbar-brand" href="index.blade.php">
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="navbar-nav">
								<li class="nav-item active">
								</li>
							</ul>
							<ul class="navbar-nav d-none d-lg-block">
								<form class="form-inline">
									<input class="form-control mr-md-2" type="text" placeholder="buscar producto">
									<a href="resultadosBusqueda.blade.php" class="btn btn-primary my-2 my-md-0">Buscar</a>
								</form>
							</ul>
							<li id="miPerfil"
								class="list-group-item d-flex d-sm-inline justify-content-between align-items-center border-0 mr-1">
								<div class="dropdown">
									<a class="btn btn-secondary dropdown-toggle " href="#" role="button"
										id="dropdownMenuCarrito" data-toggle="dropdown" aria-haspopup="true"
										aria-expanded="false">Jorge Alberto Gomez</a>
									<div class="dropdown-menu" aria-labelledby="dropdownMenuMiPerfil">
										<a class="dropdown-item" href="miPerfil.html">Ver mi perfil</a>
										<a class="dropdown-item" href="publicarProducto.html">Publicar producto</a>
										<a class="dropdown-item" href="calificarProducto.blade.php">Calificaciones</a>
										<a class="dropdown-item" href="gestionarBilletera.blade.php">Gestionar billetera</a>
										<a class="dropdown-item" href="miPuntuacion.html">Ver mi puntuación</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="index.blade.php">Cerrar sesión</a>
									</div>
								</div>
							</li>
							<ul class="navbar-nav ml-md-auto">
								<li class="list-group-item d-flex d-sm-inline justify-content-between border-0 px-0">
									<div class="form-group mb-0 pt-3">
										<div class="custom-control custom-switch">
											<input type="checkbox" class="custom-control-input" id="customSwitch1"
												checked="" onclick="cambiarPerfil();">
											<label id="textoPerfil" class="custom-control-label"
												for="customSwitch1">Vista comprador</label>
										</div>
									</div>
								</li>
								<li id="listaCarrito"
									class="list-group-item d-flex d-sm-inline justify-content-between align-items-center border-0 mr-1">
									<div class="dropdown">
										<a class="btn btn-secondary dropdown-toggle" href="#" role="button"
											id="dropdownMenuCarrito" data-toggle="dropdown" aria-haspopup="true"
											aria-expanded="false">Mi carrito<span
												class="badge badge-success badge-pill ml-1">16</span></a>
										<div class="dropdown-menu" aria-labelledby="dropdownMenuCarrito">
											<a class="dropdown-item" href="#">Detalle</a>
											<a class="dropdown-item" href="#">Vaciar</a>
										</div>
									</div>
								</li>
							</ul>
						</div>
				</nav>
				<nav>
					<ul class="navbar-nav d-md-block d-lg-none mt-3">
						<li>
							<form class="form-inline mx-3 mb-3">
								<input class="form-control mr-sm-2 w-75" type="text" placeholder="buscar producto">
								<button class="btn btn-primary my-2 my-sm-0" type="submit">Buscar</button>
							</form>
						</li>
					</ul>
				</nav>
				<div class="jumbotron">
					<h1 class="display-3 text-right">Bienvenido</h1>
					<p class="lead text-right">En esta web vas a encontrar lo que necesitas en pocos clicks, animate!
					</p>
					<hr class="my-4">
					<p class="text-right">Comenza a navegar y agrega lo que necesites a tu carrito</p>
				</div>
			</div>
		</div>
		<div id="publicacion" class="row mx-2">
			<div class="col-sm-12 col-lg-6 mb-4">
				<div id="myCarousel" class="carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ul class="carousel-indicators">
						<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
						<li data-target="#myCarousel" data-slide-to="1"></li>
						<li data-target="#myCarousel" data-slide-to="2"></li>
					</ul>

					<!-- The slideshow -->
					<div class="carousel-inner">
						<div class="carousel-item active">
							<img src="../img/imagen1.jpg" alt="Los Angeles" width="1100" height="500">
						</div>
						<div class="carousel-item">
							<img src="../img/imagen2.png" alt="Chicago" width="1100" height="500">
						</div>
						<div class="carousel-item">
							<img src="../img/imagen3.png" alt="New York" width="1100" height="500">
						</div>
					</div>

					<!-- Left and right controls -->
					<a class="carousel-control-prev" href="#myCarousel" data-slide="prev">
						<span class="carousel-control-prev-icon"></span>
					</a>
					<a class="carousel-control-next" href="#myCarousel" data-slide="next">
						<span class="carousel-control-next-icon"></span>
					</a>
				</div>
			</div>
			<div class="col-sm-12 col-lg-6">
				<div class="row">
					<div class="col-12">
						<div class="d-flex justify-content-between">
							<div></div>
							<div>
								<h4 class="d-block">Bicicleta rodado 26</h4>
								<h2 class="text-right">$1469,99</h2>
							</div>
						</div>
					</div>
				</div>
				<hr />
				<div class="row">
					<div class="col-12">
						<p class="text-primary">Lorem ipsum dolor sit amet consectetur adipiscing elit, varius primis
							sapien pharetra mauris in eget, est praesent viverra dignissim mi senectus. Quisque rhoncus
							enim fusce magna odio feugiat dignissim in, posuere praesent dis leo nec at lacus, felis ac
							ligula primis sem risus dictum. Himenaeos feugiat erat porttitor duis interdum justo ut
							varius nam habitasse, magnis vulputate pulvinar tempus eros faucibus torquent nulla taciti
							fames massa, ante eget dis donec senectus posuere nisl arcu sem.Ac sociis dui magnis
							habitasse lacinia aptent urna vitae, nunc accumsan facilisis lobortis enim potenti. Pulvinar
							arcu nulla turpis ridiculus eu at felis morbi, sollicitudin dictum non metus neque a
							integer, egestas posuere eget dapibus dui eros pretium. Lacinia integer condimentum
							venenatis tempor vulputate viverra platea montes placerat, at proin commodo dui eget
							convallis dis metus, felis cum fames scelerisque sed iaculis aliquet maecenas.</p>
					</div>
				</div>
				<hr />
				<div class="row">
					<div class="col-12">
						<div class="row">
							<div class="col-10">
								<input type="range" class="custom-range pt-4" />
							</div>
							<div class="col-2">
								<input type="text" class="form-control" id="inputdefault" value="18">
							</div>
						</div>
					</div>
				</div>
				<button type="button" class="btn btn-success btn-block mt-2"><span
						class="glyphicon glyphicon-gift glyphicon-lg mx-2"></span>Comprar</button>
			</div>
		</div>
		<hr class="mx-5 mt-5" />
		<h4 class="mx-5 text-secondary">Comentarios</h4>
		<div class="row mx-4">
			<div class="col-9">
				<div class="list-group">
					<a href="#"
						class="list-group-item-action flex-column align-items-start text-decoration-none p-3 border-bottom">
						<div class="d-flex w-100 justify-content-between">
							<small>12/09/2019</small>
						</div>
						<p class="mb-1">Comentario que hice para la publicacion porque me gusto bastante</p>
						<small>Donec id elit non mi porta.</small>
					</a>
					<a href="#"
						class="list-group-item-action flex-column align-items-start text-decoration-none p-3 border-bottom">
						<div class="d-flex w-100 justify-content-between">
							<small>12/09/2019</small>
						</div>
						<p class="mb-1">Comentario que hice para la publicacion porque me gusto bastante</p>
						<small>Donec id elit non mi porta.</small>
					</a>
					<a href="#"
						class="list-group-item-action flex-column align-items-start text-decoration-none p-3 border-bottom">
						<div class="d-flex w-100 justify-content-between">
							<small>12/09/2019</small>
						</div>
						<p class="mb-1">Comentario que hice para la publicacion porque me gusto bastante</p>
						<small>Donec id elit non mi porta.</small>
					</a>
					<a href="#"
						class="list-group-item-action flex-column align-items-start text-decoration-none p-3 border-bottom">
						<div class="d-flex w-100 justify-content-between">
							<small>12/09/2019</small>
						</div>
						<p class="mb-1">Comentario que hice para la publicacion porque me gusto bastante</p>
						<small>Donec id elit non mi porta.</small>
					</a>
					<a href="#"
						class="list-group-item-action flex-column align-items-start text-decoration-none p-3 border-bottom">
						<div class="d-flex w-100 justify-content-between">
							<small>12/09/2019</small>
						</div>
						<p class="mb-1">Comentario que hice para la publicacion porque me gusto bastante</p>
						<small>Donec id elit non mi porta.</small>
					</a>
				</div>
			</div>
			<div class="col-3">
				<div class="row">
					<div class="col-12">
						<div class="shadow pb-2 pt-2">
							<div class="mb-3 text-center">
								<div class="card-body ">
									<h4 class="card-title-center text-center mt-1">NUBE DE PALABRAS</h4>
									<div class="form-group mt-2 text-left">
										<span class="badge badge-success badge-pill">14</span>
										<span class="badge badge-success badge-pill">hola</span>
										<span class="badge badge-success badge-pill">precios bajos</span>
										<span class="badge badge-success badge-pill">tractor</span>
										<span class="badge badge-success badge-pill">viaje</span>
										<span class="badge badge-success badge-pill">vacaciones</span>
										<span class="badge badge-success badge-pill"></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12">
						<div class="shadow pb-2 pt-2 mt-3">
							<div class="mb-3 text-center">
								<div class="card-body ">
									<h4 class="card-title-center text-center mt-1">CALIFICACIONES</h4>
									<div class="form-group mt-2 text-left">
										<span class="gylphicon glyphicon-star glyphicon-xl"></span>
										<span class="gylphicon glyphicon-star glyphicon-xl"></span>
										<span class="gylphicon glyphicon-star glyphicon-xl"></span>
										<span class="gylphicon glyphicon-star glyphicon-xl"></span>
										<span class="gylphicon glyphicon-star-empty glyphicon-xl"></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<h4 class="mx-5 text-secondary mt-4">Publicaciones relacionadas</h4>
		<div class="row mt-4 mx-5">
			<div class="col-lg-3 px-1">
				<div class="card productoHover">
					<img src="../img/imagen1.jpg" alt="Los Angeles" class="px-1" style="height: 180px">
					<div class="card-body">
						<h6 class="card-title titulo-publicacion-relacionada">Titulo de una publicacion vecina</h6>
						<h3 class="card-text text-center">$12345</h3>
					</div>
				</div>
			</div>
			<div class="col-lg-3 px-1">
				<div class="card productoHover">
					<img src="../img/imagen2.png" alt="Los Angeles" class="px-1" style="height: 180px">
					<div class="card-body">
						<h6 class="card-title titulo-publicacion-relacionada">Titulo de una publicacion vecina extensa
						</h6>
						<h3 class="card-text text-center">$12345</h3>
					</div>
				</div>
			</div>
			<div class="col-lg-3 px-1">
				<div class="card productoHover">
					<img src="../img/imagen3.png" alt="Los Angeles" class="px-1" style="height: 180px">
					<div class="card-body">
						<h6 class="card-title titulo-publicacion-relacionada">Sin titulo</h6>
						<h3 class="card-text text-center">$12345</h3>
					</div>
				</div>
			</div>
			<div class="col-lg-3 px-1">
				<div class="card productoHover">
					<img src="../img/imagen2.png" alt="Los Angeles" class="px-1" style="height: 180px">
					<div class="card-body">
						<h6 class="card-title titulo-publicacion-relacionada">Titulo de una publicacion vecina con mucho
							mas detalle para que sean varias lineas</h6>
						<h3 class="card-text text-center">$12345</h3>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 p-0 mt-3">
				<div class="jumbotron m-0">
				</div>
			</div>
		</div>
		<div class="modal fade" id="modal-container-login" role="dialog" aria-labelledby="myModallogin"
			aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="myModallogin">Iniciar sesión</h5>
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label for="exampleInputEmail1">Correo electrónico</label>
							<input type="email" class="form-control" id="exampleInputEmail1">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Contraseña</label>
							<input type="password" class="form-control" id="exampleInputPassword1">
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-outline-success btn-block">Ingresar</button>
					</div>
					<a id="modal-registrar" class="btn btn-secondary text-left sinCuenta"
						href="#modal-container-registrar" role="button" data-toggle="modal"><span class="text-info">Aún
							no tengo cuenta</span></a>
				</div>
			</div>
		</div>

		<div class="modal fade" id="modal-container-registrar" role="dialog" aria-labelledby="mymodalregistrar"
			aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="mymodalregistrar">Registrarme</h5>
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<div class="col-12 px-0">
								<div class="row">
									<div class="col-6">
										<label for="exampleinputname">Nombre</label>
										<input type="text" class="form-control" id="exampleinputname">
									</div>
									<div class="col-6">
										<label for="exampleinputsurname">Apellido</label>
										<input type="text" class="form-control" id="exampleinputsurname">
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="exampleinputbirthdate">Fecha de nacimiento</label>
							<input type="date" class="form-control" id="exampleinputbirthdate">
						</div>
						<div class="form-group">
							<label for="exampleinputemail1">Correo electrónico</label>
							<input type="email" class="form-control" id="exampleinputemail1">
						</div>
						<div class="form-group">
							<label for="exampleinputpassword1">Contraseña</label>
							<input type="password" class="form-control" id="exampleinputpassword1">
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-outline-success btn-block">Registrar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="{{ URL::asset('js/jquery.min.js') }}" type="text/javascript"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="{{ URL::asset('js/scripts.js') }}" type="text/javascript"></script>
</body>

</html>