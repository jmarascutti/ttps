<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});


// ruta de gestionar billetera
Route::get('gestionarBilletera',function(){
    return view('gestionarBilletera');
})->name('gestionarBilletera');;

// ruta de calificar compra
Route::get('calificarCompra',function(){
    return view('calificarCompra');
})->name('calificarCompra');;

// ruta de vista vendedor
Route::get('verPublicaciones',function(){
    return view('verPublicaciones');
})->name('verPublicaciones');;

// ruta de detalles de la publicación
Route::get('detallesDeLaPublicacion',function(){
    return view('detallesDeLaPublicacion');
})->name('detallesDeLaPublicacion');;

// ruta de mis publicaciones
Route::get('misPublicaciones',function(){
    return view('misPublicaciones');
})->name('misPublicaciones');;

// resultados de búsqueda
Route::get('resultadosBusqueda',function(){
    return view('resultadosBusqueda');
})->name('resultadosBusqueda');;

// nueva publicacion
Route::get('nuevaPublicacion',function(){
    return view('nuevaPublicacion');
})->name('nuevaPublicacion');;

// modificar publicacion
Route::get('modificarPublicacion',function(){
    return view('modificarPublicacion');
})->name('modificarPublicacion');;

// modificar publicacion
Route::get('carrito',function(){
    return view('carrito');
})->name('carrito');;

// ruta para crear una nueva calificacion
Route::post('/movimiento/crear','BilleteraController@store')->name('movimiento.crear');

// ruta para crear una nueva calificacion
Route::post('/calificacion/crear','PagesController@crearCalificacion')->name('calificacion.crear');