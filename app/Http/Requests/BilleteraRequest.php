<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BilleteraRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    // VALIDACIONES DEL FORM DE BILLETERA
    public function rules()
    {
        return [
            // monto = maximo 5 caracteres. Dos para los enteros, uno para el separador, dos para los decimales
          $data = request()->validate([
            'monto' => 'required',          
            'descripcion' => 'required',
        ], [
            'monto.required' => 'El monto no puede estar vacío.',
            'descripcion.required' => 'El tipo de operación no puede estar vacío.'
            ])];
        
    }
}
