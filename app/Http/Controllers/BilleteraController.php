<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\BilleteraRequest;

use App;
use Carbon\Carbon;
class BilleteraController extends Controller
{
    public function store(BilleteraRequest $request) {
        $movimiento = new App\Movimiento;
        $movimiento->monto  = $request->monto;
        $movimiento->descripcion = $request->descripcion;     
         $fechaACt= Carbon::now();
        $movimiento->fecha = $fechaACt;

        $movimiento->idUsuario = '2';
        $movimiento -> save();
        
      
        
        return back()->with('mensaje','El movimiento ha sido guardado correctamente.');
    }
}
