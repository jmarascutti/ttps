<?php

namespace App\Http\Controllers;
use App;
use Carbon\Carbon;
use Illuminate\Http\Request;


class PagesController extends Controller
{
    // funcion para crear un nuevo movimiento dentro de la billetera virtual
    public function crearMovimiento (Request $request){
        $movimiento = new App\Movimiento;
        $movimiento->monto  = $request->monto;
        $movimiento->descripcion = $request->descripcion;
       // $newDateFormat2 = date('d/m/Y', strtotime($user->created_at))
       //$date = new DateTime(); // Toma la fecha y hora actual
      // $date->format('d/m/Y');
         $fechaACt= Carbon::now();
        $movimiento->fecha = $fechaACt;
        // Carbon::now()
        $movimiento->idUsuario = '2';
        $movimiento -> save();
      //  return back()-> ('mensaje','El movimiento ha sido guardado');
    }

    // funcion para crear una nueva calificacion a un usuario
    public function crearCalificacion (Request $request){
      $calificacion = new App\Calificacion;
      $calificacion->idUsuarioCalificador = '4';
      $calificacion->idUsuarioCalificado = '9';
      $calificacion->idProducto = '9';
      $calificacion->comentario = $request->comentario;
      $calificacion->calificacion= $request->radioButton;

      $calificacion -> save();
    }
}
